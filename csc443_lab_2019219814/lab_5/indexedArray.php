<HTML>
<HEAD><TITLE>Sorting The Months in Alphabetic order</TITLE></HEAD>
<BODY>
<CENTER>
<H2>Sorting Arrays using the <code>asort()</code> function</H2>
<?php

    $month['Alphabetic'] = array('J', 'F', 'M', 'A', 'M', 'J', 'J', 'A', 'S', 'O', 'N', 'D');
    $month['Name Month'] = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
    /* Sort the ahphabetic */
    asort($month['Alphabetic'], SORT_REGULAR);
?>
<TABLE border="1">
<TR>
    <TD><B>Alphabetic</B></TD>
    <TD><B>Name Month</B></TD>
</TR>
<?php
    foreach($month['Alphabetic'] as $key=>$alphabetic){
        echo "<TR>";
        echo "<TD>{$month['Alphabetic'][$key]}</TD><TD>{$month['Name Month'][$key]} </TD>";
        echo "</TR>";
    }
?>
</TABLE>
</CENTER>
</BODY>
</HTML>